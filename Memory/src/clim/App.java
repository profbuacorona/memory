package clim;

public interface App {
	
	/**
	 * chiamato una sola volta dalla piattaforma quando l'ambiente � pronto
	 * @param factory
	 */
	void init(Factory factory);
	
	/**
	 * chiamato dalla piattaforma ad ogni fotogramma per funzionalit� real-time, animazioni
	 * @param factory
	 */
	void update(Factory factory);

}
