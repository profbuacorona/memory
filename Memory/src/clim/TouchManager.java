package clim;

/**
 * Interfaccia per la notifica di un touch su una immagine. Per touch si intende la sequenza dito abbassato - dito alzato
 * (oppure il click del mouse)
 * @author 2015_2016
 *
 */

public interface TouchManager {
	
	/**
	 * 
	 * @param image immagine su cui � avvenuto il touch
	 */
	void touched(Image image);

}
