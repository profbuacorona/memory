package clim;
/**
 * Un immagine posizionabile nello spazio e scalabile
 * @author 2015_2016
 *
 */
public interface Image {
	
	void setPosition(int x, int y);
	
	/**
	 * permette di ingrandire o rimpicciolire l'immagine mantenendo le proporzioni.
	 * ScaleFactor deve essere > 0. 
	 * Se scaleFactor � < 1 l'immagine � rimpicciolita, se � maggiore di 1 � ingrandita
	 * @param scaleFactor
	 */
	void setScale(float scaleFactor);

	void hide();
	
	void show();
	
	void setTouchManager(TouchManager manager);
}
