package clim;

public interface Factory {
	
	/**
	 * Un'immagine si costruisce specificando il percorso di un file che contiene i dati.
	 * Formati ammessi: png, jpeg, gif.
	 * Le dimensioni dell'immagine in pixel corrispondono a quelle del file.
	 *  
	 */
	Image createImage(String path);

}
