package swingclim;

import java.awt.Container;

import javax.swing.JFrame;
import javax.swing.JPanel;

import clim.App;

public class SwingPlatform {
	
	private App app;

	public SwingPlatform(App app) {
		super();
		this.app = app;
		
		JFrame frame = new JFrame();
		Container container =  new JPanel(null); //absolute layout
		frame.setContentPane(container);
		SwingFactory factory = new SwingFactory(container);
		
		app.init(factory);
		
		frame.setSize(800, 600);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
	
	
	

}
