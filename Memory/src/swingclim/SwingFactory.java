package swingclim;

import java.awt.Container;

import clim.Factory;
import clim.Image;

public class SwingFactory implements Factory {

	private Container container;
	
	public SwingFactory(Container container) {
		
		this.container = container;
	}

	@Override
	public Image createImage(String path) {
		// TODO Auto-generated method stub
		SwingImage image = new SwingImage(container, path);
		return image;
	}

}
