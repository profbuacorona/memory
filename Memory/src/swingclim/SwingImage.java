package swingclim;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import clim.Image;
import clim.TouchManager;

public class SwingImage implements Image {
	
	private Container container;
	private JLabel label;
	private ImageIcon icon;
	
	private TouchManager manager;
	
	

	public SwingImage(Container container, String path) {
		super();
		this.container = container;
		
		System.out.println("Swing Image");
	
		icon = new ImageIcon(path);
		label = new JLabel(icon);
		container.add(label);
		label.setVisible(false);
		
		System.out.println(label.getPreferredSize());
		label.setSize(label.getPreferredSize());
		
		label.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				super.mouseClicked(e);
				
				manager.touched(SwingImage.this);
				
				System.out.println("mouse clicked");
				
				container.repaint();
			}
			
			
		});
		
		
	}

	@Override
	public void setPosition(int x, int y) {
		
		label.setLocation(x, y);

	}

	@Override
	public void setScale(float scaleFactor) {
		
		java.awt.Image image = icon.getImage();
		int width = (int) (image.getWidth(null)*scaleFactor);
		int height = (int) (image.getHeight(null)*scaleFactor);
		image = image.getScaledInstance(width, height, java.awt.Image.SCALE_DEFAULT);
		label.setIcon(new ImageIcon(image));
		label.setSize(new Dimension(width, height));
		System.out.println(label.getSize());
		

	}

	@Override
	public void hide() {

		label.setVisible(false);
//		container.invalidate();
//		container.repaint();
	}

	@Override
	public void show() {
		label.setVisible(true);
		//container.invalidate();
		//container.repaint();

	}

	@Override
	public void setTouchManager(TouchManager manager) {
		// TODO Auto-generated method stub
		
		this.manager = manager;

	}

}
