import clim.Factory;
import clim.Image;
import clim.TouchManager;
import clim.App;

public class Demo1 implements App {
	
	private float scale = 1;
	private String path = "img/01.png";
	
	@Override
	public void init(Factory factory) {
		// TODO Auto-generated method stub
		Image image1 = factory.createImage(path);
		
		
		image1.setTouchManager(new TouchManager() {
			boolean first = true;
			@Override
			public void touched(Image image) {
				// TODO Auto-generated method stub
				scale *= 0.8f;
				image.setScale(scale);
				
				
				
			}
		});
		
		image1.show();
		
	
		Image image2 = factory.createImage(path);
		
		image2.setPosition(100, 100);
		image2.show();
		
		image2.setTouchManager(new TouchManager() {
			
			int i;
			@Override
			public void touched(Image image) {
				// TODO Auto-generated method stub
				i++;
				System.out.println(i);
				if(i%2 == 0)
				image1.hide();
				else image1.show();
				
			}
		});
		
		
		
	}

	@Override
	public void update(Factory factory) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	

}
